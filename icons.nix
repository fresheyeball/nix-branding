{ pkgs             ? import <nixpkgs> {}
, faviconSizes     ? [16 32 57 76 96 128 144 192 228]
, appleTouchSizes  ? [120 152 180]
, androidIconSizes ? [96 192 256 512]
, colors, primary-color, background-color, name
}: with pkgs; with lib; with builtins;
let format = import ./format.nix { inherit pkgs; }; in rec {

toXxX = size: "${toString size}x${toString size}";

faviconHTML = writeText "favicon.html" ''
  <link rel="manifest" href="/site.webmanifest" />
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="${format.rgbToHex colors.${primary-color}}" />

  ${concatMapStrings (size: ''
  <link rel="icon" href="/favicon-${toXxX size}.png" sizes="${toXxX size}" />
  '')
  # this assertion is needed because msapplication-TileImage requires it
  (assert (builtins.elem 144 faviconSizes); faviconSizes)
  }

  ${concatMapStrings (size: ''
  <link rel="shortcut icon" href="/android-chrome-${toXxX size}.png" sizes="${toXxX size}" />
  '') androidIconSizes}

  ${concatMapStrings (size: ''
  <link rel="apple-touch-icon" href="/apple-touch-icon-${toXxX size}.png" sizes="${toXxX size}" />
  '') appleTouchSizes}

  <meta name="msapplication-TileImage" content="/favicon-144x144.png" />
  <meta name="msapplication-TileColor" content="${format.rgbToHex colors.${primary-color}}" />
  <meta name="theme-color" content="#FFFFFF" />

  <meta name="msapplication-config" content="/browserconfig.xml" />
'';


siteManifestXML = writeText "site.webmanifest" ''
  {
      "name": "${name}",
      "short_name": "",
      "icons": [
          ${builtins.concatStringsSep "," (map (size: ''
          {
              "src": "/android-chrome-${toXxX size}.png",
              "sizes": "${toXxX size}",
              "type": "image/png"
          }
          '') androidIconSizes ++ map (size: ''
          {
              "src": "/favicon-${toXxX size}.png",
              "sizes": "${toXxX size}",
              "type": "image/png"
          }
          '') faviconSizes
          )}
      ],
      "theme_color": "${format.rgbToHex colors.${background-color}}",
      "background_color": "${format.rgbToHex colors.${background-color}}",
      "display": "standalone"
  }
'';


browserConfigXML = writeText "browserconfig.xml" ''
  <?xml version="1.0" encoding="utf-8"?>
  <browserconfig>
      <msapplication>
          <tile>
              <square150x150logo src="/mstile-150x150.png"/>
              <TileColor>${format.rgbToHex colors.${background-color}}</TileColor>
          </tile>
      </msapplication>
  </browserconfig>
'';


mkIcons = thumbnail: dir: ''
  ln -s ${thumbnail.svg { color = {r=0;b=0;g=0;}; }} ${dir}/safari-pinned-tab.svg

  ${concatMapStrings (size: ''
  ln -s ${thumbnail.png { color = colors.${primary-color}; width = size; height = size;}} ${dir}/android-chrome-${toString size}x${toString size}.png
  '') androidIconSizes}

  ${concatMapStrings (size: ''
  ln -s ${thumbnail.png { color = colors.${primary-color}; width = size; height = size;}} ${dir}/apple-touch-icon-${toString size}x${toString size}.png
  '') appleTouchSizes}

  ${concatMapStrings (size: ''
  ln -s ${thumbnail.png { color = colors.${primary-color}; width = size; height = size;}} ${dir}/favicon-${toString size}x${toString size}.png
  '') faviconSizes}

  ${imagemagick}/bin/convert -background none -density 384 ${thumbnail.svg { color = colors.${primary-color}; }} -define icon:auto-resize ${dir}/favicon.ico
  ln -s ${siteManifestXML} ${dir}/site.webmanifest
  ln -s ${browserConfigXML} ${dir}/browserconfig.xml
'';
}
